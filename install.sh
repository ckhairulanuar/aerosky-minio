sudo dnf makecache -y
sudo dnf install epel-release -y
sudo dnf makecache -y
# selinux setting
sudo setenforce 0
# firewall rules
sudo firewall-cmd --permanent --add-service=https
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload
# anasible
sudo dnf install ansible nano python3-pip rsync -y
sudo alternatives --set python /usr/bin/python3.8
ansible-galaxy collection install -r requirements.yml
# run playbook
ansible-playbook storage.yml

